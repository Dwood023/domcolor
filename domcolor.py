from docopt import docopt
import requests
import io
from PIL import Image 
import numpy as np
import scipy
import scipy.misc
import scipy.cluster
from sklearn.cluster import KMeans


def main():
    # 1. Receives the subject of the image as text input from the caller.
    # 1. Receives input from the caller as a colour (in R,G,B values) and a subject.
    args = docopt(
        """Usage:
            domcolor.py <subject> [<r> <g> <b>]
            domcolor.py 
        """
    )
    subject = args["<subject>"]

    # 2. Scrapes reddit for the most shared images related to that subject.
    url = f"https://api.reddit.com/search?q={subject}&sort=relevance&limit=100"

    headers = {'User-agent': 'adsf'}
    response = requests.get(url, headers=headers)
    children = response.json()["data"]["children"]
    top3 = list(filter(lambda child: child["data"]["domain"] == "i.redd.it", children))[:3]

    images = [requests.get(x["data"]["url"], headers=headers).content for x in top3]
    xs = [Image.open(io.BytesIO(i)) for i in images]
    for i in xs:
        im = i.resize((150, 150))
        # clt = KMeans(n_clusters=3) #cluster number
        # clt.fit(im)


        for x in range(0,150):
            for y in range(0,150):

                im.getpixel((x,y))
# 3. Outputs links to the top three images from reddit with the ranking being determined by the number of comments on the post with that image in it.


# 2. Scrapes reddit for the most shared images related to that subject with the specified colour, or a similar one, being dominant.

# 3. Outputs links to the top three images from reddit with that dominant colour, with the ranking being determined by the number of comments on the post with that image in it

if __name__ == "__main__":
    main()
